$(document).ready(function() {
		//Form Validation
		
		// adds an effect called "wall" to the validator
		$.tools.validator.addEffect("wall", function(errors, event) {
		
			// get the message wall
			var wall = $(this.getConf().container).fadeIn();
			
			// remove all existing messages
			wall.find("p").remove();
			
			// add new ones
			$.each(errors, function(index, error) {
				wall.append(
					"<p><strong>" +error.messages[0]+ "</strong></p>"
				);		
			});
			
		// the effect does nothing when all inputs are valid	
		}, function(inputs)  {
			
		});
		
/*
		$(".form").validator({
   effect: 'wall', 
   container: '#errors',
   
   // do not validate inputs when they are edited
   errorInputEvent: null
   
// custom form submission logic  
}).submit(function(e)  { 
   
   // when data is valid 
   if (!e.isDefaultPrevented()) {
      
      // tell user that everything is OK
      $("#errors").html("<h2>All good</h2>");
      
      // prevent the form data being submitted to the server
      e.preventDefault();
   } 
   
});
*/

$(".form").validator({ 
	position: 'top center', 
	offset: [-8, 0],
	message: '<div><em/></div>' // em element is the arrow
});

   $('.branding-bg').cycle({ cleartypeNoBg: true, slideExpr: '.slide' ,prev:   '#branding-previous', next:   '#branding-next',pager:"#branding-nav ul",
   pagerAnchorBuilder: function(idx, slide) { 
        return '<li><a href="#">' + (idx+1) + '</a></li>'; 
    } });
    
   //People
   //http://ryrych.github.com/rcarousel/

   $(".people").rcarousel({visible: 4, height: 166, width: 159, step: 1, margin: 5, speed: 400, auto: {enabled: false, direction: "next", interval: 1000}});

   
   //Get the first person and load their information dynamically
   
   if($('.people').length > 0) {
   var fp = $('.people a:not(.cloned):first');
   var purl = $('.people a:not(.cloned):first').prop('href');
    
    fp.addClass('active');
    fp.append('<span class="arrow">&nbsp;</span>');
  	$.ajax({
	  url: purl,
	  beforeSend:function(){
	  	$('.cycle-info').html('Loading…');
	  	
	  },
	  success: function(data){
	  	$('.cycle-info').fadeIn(function(){
	  		//$('html,body').animate({scrollTop: $(".cycle-info").offset().top }, 2000);
	  		$(this).html(data);
	  	});
	  }
	});

   }
   //Loading Details for all Carousel via AJAX
   $('.cycle-thumb').live('click',function(){
   	
   	var clickedURL = $(this).prop('href');
   	$('.cycle-thumb').removeClass('active');
   	$(this).addClass('active');
   	$('.arrow').remove();
   	$(this).find('.arrow').remove();
   	$(this).append('<span class="arrow">&nbsp;</span>');
   	$.ajax({
		  url: clickedURL,
		  beforeSend:function(){
		  	$('.cycle-info').html('Loading…');
		  	
		  },
		  success: function(data){
		  	$('.cycle-info').fadeIn(function(){
		  		//$('html,body').animate({scrollTop: $(".cycle-info").offset().top }, 2000);
		  		$(this).html(data);
		  	});
		  }
		});
   	
   	return false;
   
   });
   
   //Close Cycle Info Window
		$('.close-window ').live('click',function(){
			$('.cycle-info').fadeOut(function(){$('.arrow').remove();
		});
   		return false;
   });
   
   
   //Volunteer Wall
   
   $('.volunteer-wall-button').live('click',function(){
   			var p = $(this).parent('aside');
   			var u = $(this).prop('href');
			// Select just Internet Explorer 7
			if (jQuery.browser.msie && jQuery.browser.version == '7.0') {
				var o = 0;
			} else {
			
				var o = 0.75;
			}
   			
				p.expose({
					color:"#000",
					opacity: o,
					// before masking begins ...
					onBeforeLoad: function(event) {
						p.addClass('exposed');
						
						$.ajax({
						  url: u,
						  beforeSend:function(){
						  	$('.exposed').nextUntil('.one').filter('section').html('Loading');
						  	
						  	
						  },
						  success: function(data){
						  $('.exposed').nextUntil('.one').filter('section').eq(0).slideDown('fast',function(){
						  		$(this).html(data);
						  		$('html,body').animate({scrollTop: $(this).offset().top }, 2000);
						  });
						  	
						 }
						});
						
					},
					onBeforeClose: function(event) {
						$('.exposed').nextUntil('.one').filter('section').html('');
						$('.exposed').nextUntil('.one').filter('section').slideUp();
						p.removeClass();
					}
			});   
   	return false;
   });
   
   $('.volunteer-close').live('click',function(){
   		$.mask.close();
   	return false;
   });
   
}); //End Ready

//Initial load of page
//$(document).ready(sizeContent);

//Every resize of window
//$(window).resize(sizeContent);

//Dynamically assign height
/*
function sizeContent() {
    var newHeight = $("html").height() - $('.header').height() - $('#branding').height() - $('.footer').height() + "px";
    var finalHeight =  $(".home-sub").height();
     if(newHeight < '273') {
     	
     	$(".home-sub").css("height", "273");
     }	else {
     	$(".home-sub").css("height", newHeight);
     }
}
*/
  