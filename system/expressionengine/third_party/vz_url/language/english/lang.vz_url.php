<?php

$lang = array(

// Field Settings

'vz_url_error_text'             => 'That URL appears to be invalid.',
'vz_url_redirect_text'          => '{{old_url}} redirects to {{new_url}}, {{update="update it"}}.',
'vz_url_nonlocal_text'          => 'Needs to be a URL on this website.',

'field_preferences'             => 'Field settings',
'vz_url_limit_local_label'      => 'Limit to local URLs',
'yes'                           => 'Yes',
'no'                            => 'No',

''=>''
);

/* End of file lang.vz_url.php */