<?php

/**
 *  CG FileInfo Plugin
 *
 * @package		ExpressionEngine
 * @subpackage	Plugins
 * @category	Plugins
 * @author    	Conflux Group, Inc. (Jeremy Gimbel) <support@confluxgroup.com>
 * 
 */

// Plugin info
$plugin_info = array(
    'pi_name' => 'CG FileInfo',
    'pi_version' => '1.0',
    'pi_author' => 'Conflux Group, Inc. (Jeremy Gimbel)',
    'pi_author_url' => 'http://addons.confluxgroup.com/fileinfo/',
    'pi_description' => 'CG FileInfo lets you access native file manager meta data from inside the Channel Entries loop',
    'pi_usage' => Cg_fileinfo::usage()
);

class Cg_fileinfo
{
    public $return_data = "";
    
    private $par_file = "";
	private $site_id;
	private $EE;
    
	/*
	 * 	Constructor
	 * 	The constructor is the only public method of the Cg_fileinfo class. It acts
	 *	as the main handler for the plugin's template tags.
	 */
    public function __construct()
    {
        // Get the EE instance
		$this->EE =& get_instance();

		// Get the current site_id
		$this->site_id = $this->EE->config->config['site_id'];
   
        // Get the file parameter from the template tag
		$this->par_file = $this->EE->TMPL->fetch_param('file');
		
		// Check if the file parameter exists.
		if(!$this->par_file)
		{
			// If there's no file parameter or it's blank, stop execution and return nothing.
			$this->return_data = '';
		}
		// Else, the file parameter is specified.
		else
		{
			
			// Parse the file parameter to get filename and path
			$filename = basename($this->par_file);
			$path = str_replace($filename, '', $this->par_file);
			
			// Find the file upload location id
			$this->EE->db->select('id');
			$this->EE->db->where('site_id', $this->site_id);
			$this->EE->db->where('url', $path);
			$query = $this->EE->db->get('exp_upload_prefs');
			$upload_pref = $query->row('id');
			
			// Find the file with the upload location id and the filename
			$this->EE->db->where('site_id', $this->site_id);
			$this->EE->db->where('upload_location_id', $upload_pref);
			$this->EE->db->where('file_name', $filename);
			
			$query = $this->EE->db->get('exp_files');
			$file_row = $query->row();
		
			// Create the template variables array
			$vars = array();
			
			// Load up the array with all the variables
			$vars['file'] = $this->par_file;
			$vars['human_size'] = $this->_human_size($file_row->file_size);
			$vars['size'] = $file_row->file_size;
			$vars['mime_type'] = $file_row->mime_type;
			$vars['modified_date'] = $file_row->modified_date;
			$vars['extension'] = pathinfo($filename, PATHINFO_EXTENSION);
			$vars['file_name'] = $filename;
			$vars['caption'] = $file_row->caption;
			$vars['is_image'] = $this->_is_image($file_row->mime_type);
			$vars['upload_date'] = $file_row->upload_date;

			
			// Determine if it's an image before adding any further variables
			if($this->_is_image($file_row->mime_type) === TRUE)
			{
				// Check if we have dimensions in our database reocrd
				if($file_row->file_hw_original)
				{
					// Set variables for the original dimensions.
					$original_dimensions = explode(' ', $file_row->file_hw_original);
					$vars['height'] = $original_dimensions[0];
					$vars['width'] = $original_dimensions[1];
				} // end check for dimensions
				
				// Get all the sizes associated with this upload location, so we
				// can generate variables for all of them as well
				$this->EE->db->select('short_name');
				$this->EE->db->where('upload_location_id', $upload_pref);
				$query = $this->EE->db->get('exp_file_dimensions');
			
				// Loop through the associated sizes to generate variables
				foreach($query->result() as $dimension)
				{
					// Assemble the URL of the manipulations, using the original path, short name of the dimension and filename
					$vars[$dimension->short_name] = $path . '_' . $dimension->short_name . '/' . $filename;
				}
							
			} // end if file is an image

			// Send our variables off to the template engine
			$this->return_data = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $vars);		

		} // end of else
			
    } // end of __construct()

	// -------------------------------------------------------------------

	/*
	 * 	Human Size
	 * 	Converts the file size, which is provided in bytes, into something more useful
	 */
	private function _human_size($filesize)
	{
		$decr = 1024;
		$step = 0;
		$prefix = array('Byte','KB','MB','GB','TB','PB');

		while( ($filesize / $decr) > 0.9)
		{
			$filesize = $filesize / $decr;
			$step++;
		}
		
		return round($filesize,2). ' ' . $prefix[$step];
	
	} // end _human_size()
    
	// -------------------------------------------------------------------

	/*
	 * 	Is Image
	 * 	Determines if a given mimetype is an image
	 */
	private function _is_image($mime)
	{
		$viewable_image = array('image/bmp','image/gif','image/jpeg','image/jpg','image/jpe','image/png');
		
		$viewable = (in_array($mime, $viewable_image)) ? TRUE : FALSE;
		
		return 	$viewable;	
	
	} // end _is_image()
    
    // -------------------------------------------------------------------

    /*
	 * 	Usage
	 * 	Displays usage information about the plugin.
	 *
	 */
    
    public function usage()
    {
        ob_start();
?>
	CG FileInfo is an ExpressionEngine plugin which allows you to access the native File Manager's
	meta data for a given file.
	
	Simply use the following tag pair, with the required file parameter.
	
	{exp:cg_fileinfo file="{custom_field_name}"}
	
	{/exp:cg_fileinfo}
	
	The following variables are available within the FileInfo tag pair:
	
	{caption}
	{extension}
	{file}
	{file_name}
	{height}
	{human_size}
	{mime_type}
	{modified_date}
	{size}
	{upload_date}
	{width}

	For more detailed documentation, visit the online documentation at: http://addons.confluxgroup.com/fileinfo/.



<?php
		$buffer = ob_get_contents();
        
		ob_end_clean();
        
		return $buffer;
    
	} // end of usage()
    
} // end of Cg_fileinfo

/* End of file pi.cg_fileinfo.php */
/* Location: ./system/expressionengine/third_party/cg_fileinfo/pi.cg_fileinfo.php */