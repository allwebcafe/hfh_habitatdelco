<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
  'pi_name' => 'URL Decode',
  'pi_version' => '1.0',
  'pi_author' => 'Ahmad Saad',
  'pi_author_url' => '',
  'pi_description' => 'Runs urldecode on a segment.',
  'pi_usage' => URL_decode::usage()
  );

/**
 * URL_decode Class
 *
 * @package			ExpressionEngine
 * @category		Plugin
 * @author			Ahmad Saad
 * @copyright		Copyright (c) 2012, Ahmad Saad
 * @link			
 */

class URL_decode {

var $return_data = "";

	// --------------------------------------------------------------------

	/**
	 * URL_decode
	 *
	 * This function runs urldecode() on a string of text
	 *
	 * @access	public
	 * @return	string
	 */


  function URL_decode()
  {
	$this->EE =& get_instance();
	$this->return_data = urldecode($this->EE->TMPL->tagdata);
  }
  
  	// --------------------------------------------------------------------

	/**
	 * Usage
	 *
	 * This function describes how the plugin is used.
	 *
	 * @access	public
	 * @return	string
	 */
	
  //  Make sure and use output buffering

  function usage()
  {
  	ob_start(); 
  ?>
The URL Decode plugin urldecode your Url format string to make it readable string.

The tag pair is simply:
{exp:url_decode}Url format string{/exp:url_decode}

Example usage:
URl = http://www.example.com/New Jersey
{segment_1} will return New%20Jersey 
 
{exp:url_decode}{segment_1}{/exp:url_decode}

	becomes:
	
New Jersey



  <?php
  	$buffer = ob_get_contents();
	
  	ob_end_clean(); 

  	return $buffer;
  }
  // END

  

}

/* End of file pi.url_decode.php */ 
/* Location: ./system/expressionengine/third_party/url_decode/pi.url_decode.php */
